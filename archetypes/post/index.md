---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
weight: 0
tags: []
summary: ""
cover:
    image: "img/header.jpg"
    alt: "Header image alt text"
    caption: "header image caption"
    relative: true
---